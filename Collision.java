package Package;

import java.awt.Rectangle;

public class Collision extends Rectangle {
	private static final long serialVersionUID = 1L;
	private boolean isOn;
	
	public Collision(int x, int y, int width, int height){
		super(x, y, width, height);
		isOn = true;
	}
	public boolean checkCollision(Collision collisorRect){ 
		if(this.intersects(collisorRect.getBounds()) && isOn){
			return true;
		}else{
			return false;
		}
	}
	public void turnOffCollision(){
		this.isOn = false;
	}
	public Collision getCollision(){
		if (isOn) {
			return this;
		}else{
			this.setBounds(2000, 2000, 0, 0);
			return this;
		}
		
	}
}
