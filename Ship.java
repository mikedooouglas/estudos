package Package;

import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Ship extends JLabel{
	private static final long serialVersionUID = 1L;
	private Game game;
	private int life, speed;
	private boolean leftDirection, rightDirection;
	private ArrayList<Shot> shot;
	private Collision collision;
	private boolean isDestroied;

	
	public Ship(Game game, int x, int y) {
		setBounds((x - 48), y, 96 ,94);
		this.game = game;
		isDestroied = false;
		shot = new ArrayList<Shot>();
		speed = 6;
		collision = new Collision(this.getX(), this.getY(), this.getWidth(), this.getHeight());
		setIcon(new ImageIcon("img\\spaceship.png"));
	}
	public void shoot(){
		Shot tempShot = new Shot("ship", this.getX() + (this.getWidth() / 2), this.getY());
		shot.add(tempShot);
		game.getLabelMain().add(tempShot);
	}
	public ArrayList<Shot> getShot(){
		return shot;
	}
	public void movement(){
		if(leftDirection && this.getX() > 0){
			setLocation(this.getX() - speed, this.getY());
			collision.setLocation(this.getX(), this.getY());
		}
		if (rightDirection && this.getX() < 550 - this.getWidth()){
			setLocation(this.getX() + speed, this.getY());
			collision.setLocation(this.getX(), this.getY());
		}
	}
	public void activateMovement(String direction){
		if(direction.equals("left")){
			this.leftDirection = true;
		}else if(direction.equals("right")){
			this.rightDirection = true;
		}
	}
	public void deactivateMovement(String direction){
		if(direction.equals("left")){
			this.leftDirection = false;
		}else if(direction.equals("right")){
			this.rightDirection = false;
		}
	}
	public boolean checkCollision(Collision otherCollision){
		return this.collision.checkCollision(otherCollision);
	}
	public Collision getCollision(){
		return collision;
	}
	public void moveShot(){
		for (int i = 0; i < shot.size(); i++) {
			this.shot.get(i).move();
		}
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public int getLife() {
		return life;
	}
	public void setLife(int life) {
		this.life = life;
	}
	public boolean isDestroied() {
		return isDestroied;
	}
	public void setDestroied() {
		this.isDestroied = true;
	}
}