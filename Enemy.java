package Package;

import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Enemy extends JLabel{
	private static final long serialVersionUID = 1L;
	private Game game;
	private int life, speed;
	private ArrayList<Shot> shot;
	private Collision collision;
	private boolean isDestroied;
	
	public Enemy(Game game, int x, int y) {
		this.game = game;
		isDestroied = false;
		setBounds(x, y, 96, 94);
		shot = new ArrayList<Shot>();
		setIcon(new ImageIcon("img\\enemy.png"));
		collision = new Collision(x, y, this.getWidth(), this.getHeight());
		speed = 3;
	}
	public void movement(){
		this.setLocation(this.getX(), this.getY() + speed);
		collision.setLocation(this.getX(), this.getY());
	}
	public void moveShot(){
		for (int i = 0; i < shot.size(); i++) {
			this.shot.get(i).move();
		}
	}
	public void shoot(){
		Shot tempShot = new Shot("enemy", this.getX() + (this.getWidth() / 2), this.getY() + this.getHeight());
		shot.add(tempShot);
		game.getLabelMain().add(tempShot);
	}
	public ArrayList<Shot> getShot(){
		return shot;
	}
	public void destroyShotArray(){
		this.shot = null;
	}
	public boolean checkCollision(Collision otherCollision){
		return this.collision.checkCollision(otherCollision);
	}
	public Collision getCollision(){
		return this.collision.getCollision();
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public int getLife() {
		return life;
	}
	public void setLife(int life) {
		this.life = life;
	}
	public boolean isDestroied() {
		return isDestroied;
	}
	public boolean getIsDestroied(){
		return isDestroied;
	}
	public void setDestroied() {
		this.collision.turnOffCollision();
		this.isDestroied = true;
	}
}