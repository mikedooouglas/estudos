package Package;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Game extends JFrame implements KeyListener{
	private static final long serialVersionUID = 1L;
	private Ship ship;
	private float timeToUpdateGame, timerGameDestroier;
	private ArrayList<Enemy> enemy;
	private JPanel panel;
	private JLabel labelBackground;
	private Timer time;
	private JLabel labelMain;
	
	public Game(){
		//this.setUndecorated(true);
		this.setResizable(false);
		this.setVisible(true);
		this.setSize(550, 650);
		this.setLayout(null);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		timerGameDestroier = 0;
		timeToUpdateGame = 25; // TEMPO DE ATUALIZAÇÃO DO JOGO EM MILISSEGUNDOS
		
		panel = new JPanel();
		panel.setBounds(0, 0, 550, 650);
		panel.setLayout(null);
		this.add(panel);
		
		labelMain = new JLabel();
		labelMain.setBounds(0, 0, 550, 650);
		panel.add(labelMain);
		
		labelBackground = new JLabel();
		labelBackground.setBounds(0, -650, 550, 1300);
		labelBackground.setIcon(new ImageIcon("img\\bg.jpg"));
		panel.add(labelBackground);	
		
		enemy = new ArrayList<Enemy>();
		
		ship = new Ship(this, (this.getWidth()/2), this.getHeight() - 140);
		labelMain.add(ship);
		
		updateGame();
		addKeyListener(this);
		
		repaint();
	}
	public void updateGame(){
		time = new Timer();
		time.schedule(new TimerTask(){

			@Override
			public void run() {
				moveBackground();
				ship.moveShot();
				moveEnemyShot();
				ship.movement();
				enemyMovement();
				enemySpawner();
				checkGameCollisions();
				enemyShoot();
				
				timerGameDestroier += timeToUpdateGame;
				if (timerGameDestroier > 1000 * 4) {
					gameDestroier();
				}
			}
		}, 0, (long)timeToUpdateGame);
	}
	public void enemyMovement(){
		for (int i = 0; i < enemy.size(); i++) {
			enemy.get(i).movement();
		}
	}
	public void enemySpawner(){
		double percentSpawnEnemy = Math.random() * 100;
		if(percentSpawnEnemy < 1.5){
			Random random = new Random();
			int randomPositionX = random.nextInt(550);
			if(randomPositionX > 50 && randomPositionX < 500){
				Enemy tempEnemy = new Enemy(this, randomPositionX, -100);
				this.labelMain.add(tempEnemy);
				this.enemy.add(tempEnemy);
				System.out.println(this.enemy.size());
			}
		}
	}
	public void checkGameCollisions(){
		// Colision Player X Enemies
		for (int i = 0; i < enemy.size(); i++) {
			if(ship.checkCollision(enemy.get(i).getCollision())){
				gameOver();
			}
		}
		// Collision Enemy X Shot
		for (int i = 0; i < enemy.size(); i++) {
			for (int j = 0; j < ship.getShot().size(); j++) {
				if(enemy.size() > 0){
					if(enemy.get(i).getCollision().checkCollision(ship.getShot().get(j).getCollision())){
						destroyEnemy(enemy.get(i));
						destroyShipShot(j);
						break;
					}
				}
			}
		}
	}
	public void moveEnemyShot(){
		for (int i = 0; i < enemy.size(); i++) {
			enemy.get(i).moveShot();
		}
	}
	public void gameDestroier(){
		timerGameDestroier = 0;
		for (int i = 0; i < enemy.size(); i++) { // for to destroy enemies;
			if (enemy.get(i).getY() > this.getHeight() + 200) {
				enemy.get(i).destroyShotArray();
				labelMain.remove(enemy.get(i));
				this.enemy.remove(enemy.get(i));
				
			}
		}
		for (int i = 0; i < ship.getShot().size(); i++) {
			if (ship.getShot().get(i).getY() > this.getHeight() + 200) {
				labelMain.remove(ship.getShot().get(i));
				ship.getShot().remove(i);
			}
		}
	}
	public void destroyEnemy(Enemy enemy){
		enemy.setVisible(false);
		enemy.setDestroied();
	}
	public void destroyShipShot(int shotIndex){
		labelMain.remove(ship.getShot().get(shotIndex));
		ship.getShot().remove(shotIndex);
	}
	public void moveBackground(){
		if(labelBackground.getY() < 0){
		labelBackground.setLocation(0, labelBackground.getY() + 1);
		}else{
			labelBackground.setLocation(0, -650);
		}
	}
	public void enemyShoot(){
		Random random = new Random();
		int percentToShoot = random.nextInt(100);
		if(percentToShoot < 1 && enemy.size() > 0){
			int indexEnemyToShoot = random.nextInt(this.enemy.size());
			if(!enemy.get(indexEnemyToShoot).isDestroied()){
				this.enemy.get(indexEnemyToShoot).shoot();
			}
		}
	}
	public void gameOver(){
		System.out.println("GAME OVER!");
	}
	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_SPACE || e.getKeyCode() == KeyEvent.VK_ENTER){
			ship.shoot();
		}
		if(e.getKeyCode() == KeyEvent.VK_A || e.getKeyCode() == KeyEvent.VK_LEFT){
			ship.activateMovement("left");
		}
		if(e.getKeyCode() == KeyEvent.VK_D || e.getKeyCode() == KeyEvent.VK_RIGHT){
			ship.activateMovement("right");
		}
	}
	@Override
	public void keyTyped(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_A  || e.getKeyCode() == KeyEvent.VK_LEFT){
			ship.deactivateMovement("left");
		}
		if(e.getKeyCode() == KeyEvent.VK_D  || e.getKeyCode() == KeyEvent.VK_RIGHT){
			ship.deactivateMovement("right");
		}
	}
	public JLabel getLabelBackground() {
		return labelBackground;
	}
	public JPanel getPanel(){
		return panel;
	}
	public JLabel getLabelMain(){
		return labelMain;
	}
}