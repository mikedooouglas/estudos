package Package;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Shot extends JLabel {
	private static final long serialVersionUID = 1L;
	private int speed;
	private Collision collision;
	private String typeShot; // up or down
	private boolean isDestroied;
	
	public Shot(String typeShot, int initialX, int initialY) {
		isDestroied = false;
		speed = 7;
		setBounds(initialX - 5, initialY, 10, 20);
		this.typeShot = typeShot;
		collision = new Collision(initialX, initialY, this.getWidth(), this.getHeight());
		setTypeShot();
		setVisible(true);
	}
	public void move(){
		this.setLocation(this.getX(), this.getY() + speed);
		collision.setLocation(this.getX(), this.getY());
	}
	public void setTypeShot(){
		if(typeShot.equals("ship")){
			speed *= -1;
			this.setIcon(new ImageIcon("img\\shipShot.png"));
		}else if(typeShot.equals("enemy")){
			speed *= 1;
			this.setIcon(new ImageIcon("img\\enemyShot.png"));
		}
	}
	public boolean checkCollision(Collision otherCollision){
		return this.collision.checkCollision(otherCollision);
	}
	public Collision getCollision(){
		return collision;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public boolean isDestroied() {
		return isDestroied;
	}
	public void setDestroied() {
		this.isDestroied = true;
	}
}
